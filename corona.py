import numpy as np
import ipdb
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import datetime

# parameters -------------------------------------------------------------------
population_size = 9 * 10**6

nb_patients_zero = 1.
date_patient_zero = datetime.datetime(2020, 2, 1)
date_social_distancing = datetime.datetime(2020, 3, 13)

# expected number of people infected per sick person per day,
def contagions_per_person_per_day(date, nb_not_yet_infected):

    # ratio of people which are still not immune with respect to total
    ratio = nb_not_yet_infected / float(population_size)

    # expected transmissions per person per day.
    expected_transmissions_normal = 0.6 # <======== critical parameter
    expected_transmission_social_distancing = 0.3 # <======== critical parameter

    if date < date_social_distancing:
        return expected_transmissions_normal * ratio
    else:
        return expected_transmission_social_distancing * ratio

# after being infected it takes some days to become contagious
days_til_contagious = 3 # <======== critical parameter
# after detection, no other person will be infected from that patient
days_til_detection = 7 # <======== critical parameter

date_start_plot = datetime.datetime(2020, 3, 1)
date_end_plot = datetime.datetime(2020, 10, 15)

# simulation -------------------------------------------------------------------
def date_to_index(date):
    return (date - date_patient_zero).days + days_til_detection


def index_to_date(index):
    return date_patient_zero + datetime.timedelta(days=index -
                                                  days_til_detection)


index_start = date_to_index(date_patient_zero)
index_end = date_to_index(date_end_plot)

infected_new = [0.] * index_end
infected_new[index_start] = nb_patients_zero
not_yet_infected = [population_size] * index_end
not_yet_infected[index_start] = population_size - infected_new[index_start]
detected_new = [0.] * index_end
detected_total = [0.] * index_end
contagious_total = [0.] * index_end

for t in range(index_start + 1, index_end):
    detected_new[t] = infected_new[t - days_til_detection]
    detected_total[t] = detected_total[t-1] + detected_new[t]

    contagious_total[t] = sum(
        infected_new[t - days_til_detection + 1: t - days_til_contagious + 1])

    infected_new[t] = (contagious_total[t]
                       * contagions_per_person_per_day(index_to_date(t),
                                                       not_yet_infected[t-1]))
    not_yet_infected[t] = not_yet_infected[t-1] - infected_new[t]


# plotting ---------------------------------------------------------------------
index_start_plot = date_to_index(date_start_plot)
date_list = [index_to_date(index) for index in range(index_end)]
plt.subplot(3,1,1)
plt.plot_date(date_list[index_start_plot:], detected_new[index_start_plot:])
plt.title("number of detections per day")
plt.xticks([])
plt.subplot(3,1,2)
plt.plot_date(date_list[index_start_plot:], detected_total[index_start_plot:])
plt.title("total number of detections")
plt.xticks([])
plt.subplot(3,1,3)
plt.plot_date(date_list[index_start_plot:], contagious_total[index_start_plot:])
plt.title("number of currently contagious people")

plt.setp(plt.gca().xaxis.get_majorticklabels(),
         'rotation', 90)
plt.show()

# ipdb.set_trace()
